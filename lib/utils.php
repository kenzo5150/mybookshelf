<?php

function h($str)
{
    return htmlspecialchars($str, ENT_QUOTES);
}

/**
 * Flash Messageの存在/クリアしない
 *
 * @return (string) flash_message
 */
function hasFlashMessage()
{
    $flash_message = null;
    if (array_key_exists("flash_message", $_SESSION)) {
        $flash_message = $_SESSION['flash_message'];
    }
    return isset($flash_message);
}

/**
 * Flash Message取得/クリア
 *
 * @return (string) flash_message
 */
function getFlashMessage()
{
    $flash_message = null;
    if (array_key_exists("flash_message", $_SESSION)) {
        $flash_message = $_SESSION['flash_message'];
        unset($_SESSION['flash_message']);
    }
    return $flash_message;
}

/**
 * Flash Message set
 *
 * @param [string] $str
 * @return void
 */
function setFlashMessage($str)
{
    $_SESSION['flash_message'] = $str;
}


/**
 * ログイン済みかチェック
 *
 * @return boolean true:ログイン済
 */
function isLogin($redirect = false, $message = "ログインしてください")
{
    $result = false;
    if (array_key_exists('user_id', $_SESSION)) {
        $result = true;
    } else {
        // die("login incorrect!");
    }

    if ($redirect === true && $result === false) {
        $_SESSION['flash_message'] = $message;
        header("location:./?m=login");
        exit();
    }

    return $result;
}


/**
 * 個別ディレクトリから設定を取得
 * 
 */

function get_user_prof(){

    $users = get_users();

    foreach ($users as $x) {
        $dir = "./data/" . $x;
        
        // 既知のディレクトリをオープンし、その内容を読み込み
        if (is_dir($dir)) {
            $user_prof=  json_decode(file_get_contents($dir."/"."user_prof.json"), true);
            // print_r($user_prof); //['name'];
            // print "----" . PHP_EOL ;
            // echo $x;
            $user_prof['id']=$x;
            $user_profs[$x] = $user_prof;
        } else {
            echo "error";
        }
    }
    
}

/**
 * ユーザー個別ディレクトリ取得
 * 
 * @param string $data_dir
 * @return void
 */
function get_users($data_dir=""){

    $users = [] ;

    if (is_dir($data_dir)) {
        if ($dh = opendir($data_dir)) {
            while (($file = readdir($dh)) !== false) {
                if (filetype($data_dir ."/". $file) !== "dir") {
                    continue ;
                } elseif ($file === ".") {
                    continue ;
                } elseif ($file === "..") {
                    continue ;
                } elseif (substr($file, 0, 1)!=="u") {
                    continue;
                } else {
    
    
                    // echo "filename: $file : filetype: " ." *" .  substr($file,0,1) ." " . filetype($data_dir ."/". $file) . PHP_EOL ."<br/>";
                    $users[] = $file ;
                }
            }
            closedir($dh);
        }
    }
    return $users ; 
}


/**
 * Undocumented function
 *
 * @param string $dir_name
 * @return void
 */
function get_files_in_dir($dir_name=""){

    $files = [] ;
    
    if (is_dir($dir_name)) {
        if ($dh = opendir($dir_name)) {
            while (($file = readdir($dh)) !== false) {
                if (filetype($dir_name ."/". $file) === "dir") {
                    continue ;
                } elseif ($file === ".") {
                    continue ;
                } elseif ($file === "..") {
                    continue ;
                } else {
       
                    // echo "filename: $file : filetype: " ." *" .  substr($file,0,1) ." " . filetype($dir_name ."/". $file) . PHP_EOL ."<br/>";
                    $files[] = $file ;
                }
            }
            closedir($dh);
        }
    }
    return $files ; 
}


/**
 * 管理者権限保有有無の
 *
 * @param [type] $user_role
 * @param [type] $user_id
 * @return boolean
 */
function hasAdminRole($user_role=array(), $user_id)
{
    if( !array_key_exists( $user_id, $user_role) ) 
    {
        return false ;
    }

    if( !array_key_exists('admin' , $user_role[$user_id]) )
    {
        return false ;
    }

    return ( $user_role[$user_id]['admin'] )?  true : false ;

}



/**
 * Undocumented function
 *
 * @param [array] $list
 * @param [string] $key
 * @return void
 */
function list_summarize($list, $key) {
    $res=[];
    foreach($list as $row ){
        $res[ $row[$key] ][]=$row;
    }
    return $res ;
}
