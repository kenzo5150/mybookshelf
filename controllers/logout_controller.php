<?php

isLogin(true,"ログイン認証されていません");
$page['title']="ログアウトの確認";


if ($_SERVER['REQUEST_METHOD']==="POST") {
    //var_dump($params);

    // $user_id = $params['user_id'];
    // $password = $params['password'];

    $result=false;
    if( array_key_exists('user_id', $_SESSION) ){
        $result=true;
    } else {
        // die("login incorrect!");
    }

    if ($result===true) {
        unset($_SESSION['user_id']);
        $_SESSION['flash_message']="ログアウトしました";
        header("location:./?m=login");

    } else {
        $_SESSION['flash_message']="ログアウトに失敗しました";
    }

} else{

    $flash_message = getFlashMessage();
        
}

require("templates/logout.html.php");
