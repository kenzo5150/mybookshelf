<?php
isLogin(true,"ご利用にはログイン認証が必要です。");
$book_id = $params['id'];

$book = null;
foreach ($books as $row) {
    if ($row['id'] === $book_id) {
        $book = $row;
        break;
    }
}
// var_dump($book);
$flash_message = getFlashMessage();

$book_user_info = null;
// 貸借ユーザーの特定
foreach ($book_status as $row) {
    if ($row['id'] === $book_id) {
        $book_user_info = $row;
        break;
    }
}
$page['title'] = $book['id'] . ":" . $book['title'];

$book_user = null;
if ($book_user_info) {
    $book_user = $user_profs[$book_user_info['user_id']];
}
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $data = $params;  //コピー
    unset($data['m']);

    $data['updated_at']=date("Y-m-d") ."T" . date("H:i:s");

    $temp = []; //一時バッファ
    foreach ($books as $row) {
        if ($row['id'] === $data['id']) {
            $temp[] = $data; //対象レコードは更新
            continue;
        }
        $temp[] = $row; //そのまま変更なしでコピー
    }

    $js = json_encode($temp, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
    file_put_contents(M_BOOKS_PATH, $js, LOCK_EX);
    // バックアップが有効な場合は以下を適用
    // copy(M_BOOKS_PATH,DATAFILE_PATH . "/" . "m_books_bk_" .date('YmdHis') . ".json");
    // copy(M_BOOKS_TEMP_PATH,M_BOOKS_PATH);
    setFlashMessage("保存しました");
    header("location:?m=detail&id=" . $book_id );
    exit();
} else {
    require("templates/edit.html.php");
}
