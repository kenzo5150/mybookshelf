<?php

isLogin(true,"ご利用にはログイン認証が必要です。");

$book_user_info = null;

/*
if ( array_key_exists('id', $params) ) {
    $user_id = $params['id'];
}else{
    $user_id = $_SESSION['user_id'];
}

// ユーザーのデータフォルダー
$user_folder=DATAFILE_PATH .  '/' .  $user_id ;

// ユーザーの検出
foreach ($user_profs as $row) {
    if ($row['id'] === $user_id) {
        $user = $row;
        break;
    }
}

// パラメータチェック
if( empty($user) ){
    // die("パラメータ不正、サポートしないアクセスです");
}
// var_dump($book);

// 利用ユーザーの特定
foreach ($book_status as $row) {
    if ($row['id'] === $user_id) {
        $book_user_info = $row;
        break;
    }
}
*/


$book_user = null;
if ($book_user_info) {
    $book_user = $user_profs[$book_user_info['user_id']];
}

// 保存
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $data = $params;  //コピー
    unset($data['m']);
    unset($data['password_c']);

    $temp = []; //一時バッファ
    $data['img']="common/user_default.jpg";
    $data['updated_at']=date("Y-m-d") ."T" . date("H:i:s");
    $data['password']=md5($data['password']);
    // $data['user_id']=$_SESSION['user_id'];
    $data['id']='u' . sprintf("%03d",  (count($user_profs) + 1)) ;//ID自動付与

    // 末尾レコード追加
    $user_id=$data['id'];
    // @TODO:重複チェック
    $user_profs[$user_id]=$data;//ユーザーマスタ

    $js = json_encode($user_profs, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    file_put_contents(M_USERS_PROF_PATH, $js, LOCK_EX);
    mkdir( DATAFILE_PATH."/". $user_id, "0777" );//ディレクトリ作成
    setFlashMessage("保存しました");
    header("location:?m=edit_members&id=" . $user_id );
    exit();
} else {

    $flash_message = getFlashMessage();
    $page['title'] =  "新規メンバー登録";
    $file_in_dir=[];
    // $file_in_dir = get_files_in_dir($user_folder);
    $user = [
        'id'=>'',
        'name'=>'noname',
        'password'=>'',
        'img'=>'common/user_default.jpg',
        'mail'=>'default@local.local',
        
    ];
    
    // echo "<pre>"; print_r($file_in_dir); die();//
    require("templates/add_members.html.php");
}
