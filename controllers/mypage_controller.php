<?php

isLogin(true,"ご利用にはログイン認証が必要です。");// ログイン済みチェック

//---------------------------------------

// パラメータで対象ユーザーを選択可能、なければセッションユーザー自身
if ( array_key_exists('id', $params) ) {
    $user_id = $params['id'];
}else{
    $user_id = $_SESSION['user_id'];
}


// パラメータチェック
if( empty($user_id)  || ! array_key_exists($user_id, $user_profs) ){
    setFlashMessage("無効なアクセス/パラメータです");    
    header("location:?m=login");
    exit();
}

//---------------------------------------

$page['title']="My Page";

// 自身が利用している書籍リスト取得
$my_book_status=[];
$my_book_keys=[];
$my_book_list=[];
foreach( $book_status as $row ){
    if( $row['user_id'] === $user_id ){
        $my_book_status[$row['id']] = $row ;
        $my_book_keys[] = $row['id'] ;
    }
}
// 利用書籍詳細の一覧取得
foreach( $books as $row ){
    if(  in_array( $row['id'],$my_book_keys,true) ){
        $my_book_list[$row['id']] = $row ;
    }
}

//print_r( $my_book_list); die();

// 自身が投稿したレビューコメント取得
$my_book_review_keys=[];
$my_book_comments=[];
$my_book_review_list=[];
foreach( $book_review_list as $row ){
    if( $row['user_id'] === $user_id ){
        $my_book_comments[] = $row ;
        $my_book_review_keys[] = $row['book_id'] ;
    }
}

$my_book_review_keys= array_unique($my_book_review_keys);
// レビューコメントした書籍詳細の一覧取得
foreach( $books as $row ){
    if(  in_array( $row['id'],$my_book_review_keys,true) ){
        $my_book_review_list[$row['id']] = $row ;
    }
}

//

$user_prof = $user_profs[$user_id] ;

$flash_message = getFlashMessage();

require("templates/mypage.html.php");
