<?php
isLogin(true, "ご利用にはログイン認証が必要です。");

$book_id = $params['id'];
$user_id = $params['user_id'];
$date_to = $params['date_to'];


if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $data = $params;  //コピー
    unset($data['m']);

    $data['updated_at'] = date("Y-m-d") . "T" . date("H:i:s");

    $temp = []; //一時バッファ
    foreach ($book_status as $row) {
        if ($row['id'] === $data['id']) {
            continue;
        }
        $temp[] = $row; //そのまま変更なしでコピー
    }

    // ユーザー,期限日付が指定されている場合は追加、指定ない場合はキー削除
    if ($user_id !== "" && $date_to!=="") {
        $temp[] = $data; //対象レコードは更新
    }

    // var_dump($data); die("end");
    $js = json_encode($temp, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    file_put_contents(T_BOOKS_STATUS_PATH, $js, LOCK_EX);
    // バックアップが有効な場合は以下を適用
    // copy(M_BOOKS_PATH,DATAFILE_PATH . "/" . "m_books_bk_" .date('YmdHis') . ".json");
    // copy(M_BOOKS_TEMP_PATH,M_BOOKS_PATH);
    setFlashMessage("保存しました");
    header("location:?m=detail&id=" . $params['id']);
    exit();
} else {
    die("許可されていないアクセスです");
    // require("templates/edit.html.php");
}
