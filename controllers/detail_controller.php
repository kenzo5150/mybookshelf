<?php

isLogin(true,"ご利用にはログイン認証が必要です。");

$book_id=$params['id'];

$book = null;
foreach($books as $row){
    if($row['id']===$book_id){
        $book = $row;
        break;
    }
}


$book_user_info=null;
// 貸借ユーザーの特定
foreach( $book_status as $row){
    if($row['id']===$book_id){
        $book_user_info=$row;
        break;
    }
}
$page['title']=$book['title'];

$book_user=null;
if ($book_user_info) {
    $book_user= $user_profs[$book_user_info['user_id']] ;
}
$flash_message = getFlashMessage();

require("templates/detail.html.php");
