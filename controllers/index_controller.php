<?php

isLogin(true, "ご利用にはログイン認証が必要です。");

$page['title'] = "みんなの書棚";

$flash_message = getFlashMessage();

// 登録件数
$total_count = count($books);


// 検索
$flg_title = null;
$flg_notes = null;
$result_list = []; //検索結果を加味したレコードリストバッファー


if (
    array_key_exists('k_title', $params)
    ||
    array_key_exists('k_note', $params)
) {
    foreach ($books as $row) {
        
        $flg_title = (array_key_exists('k_title', $params) && preg_match( "/". quotemeta( $params['k_title']) ."/i", $row['title']  ) )?  true : null ;
        $flg_notes = (array_key_exists('k_notes', $params) && preg_match( "/". quotemeta( $params['k_notes']) ."/i", $row['notes']  ) )? true : null ;

        // いずれかがマッチしていれば検索結果リストに追加
        if ( ( ($flg_title) ) &&  ( ($flg_notes)) ) {
            // var_dump($flg_title .":". $row['title']); //die();
            $result_list[] = $row;
        }
        
    }

} else {
    $result_list = $books;
}


require("templates/index.html.php");
