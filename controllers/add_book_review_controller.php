<?php

isLogin(true,"ご利用にはログイン認証が必要です。");

$book_id = $params['book_id'];

// 保存
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $data = $params;  //コピー
    unset($data['m']);

    $data['date_at']=date("Y/m/d") ."T" . date("H:i:s");
    $data['user_id']=$_SESSION['user_id'];

    // 末尾レコード追加
    $book_review_list[]=$data;

    $js = json_encode($book_review_list, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    // var_dump($js);    die("test end"); //for debug

    file_put_contents(T_BOOKS_REVIEW_PATH, $js, LOCK_EX);
    // バックアップが有効な場合は以下を適用
    // copy(M_USERS_PROF_PATH,DATAFILE_PATH . "/" . "m_user_prof"."_bk" .date('YmdHis') . ".json");
    // copy(M_USERS_PROF_TEMP_PATH,M_USERS_PROF_PATH);
    setFlashMessage("保存しました");
    header("location:?m=detail&id=" . $book_id );
    exit();
} else {
    die("許可されていないアクセスです");
}
