<?php

isLogin(true,"ご利用にはログイン認証が必要です。");

$page['title']="新規登録";


// 保存
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $data = $params;  //コピー
    unset($data['m']);

    $data['updated_at']=date("Y-m-d") ."T" . date("H:i:s");
    $data['user_id']=$_SESSION['user_id'];
    $data['id']='gi' . (count($books) + 1) ;

    // 末尾レコード追加
    $books[]=$data;

    $js = json_encode($books, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    $book_id=$data['id'];
    file_put_contents(M_BOOKS_PATH, $js, LOCK_EX);
    setFlashMessage("保存しました");
    header("location:?m=detail&id=" . $book_id );
    exit();
} else {
    $book=[
        'id'=>'',
        'title'=>'',
        'img'=>'',
        'released_at'=>'',
        'notes'=>'',
        'url'=>'',
        'title'=>'',
    ];
    $book_user=null;
    $flash_message = getFlashMessage();
    
    require("templates/add_book.html.php");
}
