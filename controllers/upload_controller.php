<?php
isLogin(true,"ご利用にはログイン認証が必要です。");// ログイン済みチェック

$page['title']="ユーザーフォルダ アップロード";

//---- 通常ユーザーはデフォルトパラメータ無しで自身を対象とする。パラメータで選択可能

if ( array_key_exists('id', $params) ) {
    $user_id = $params['id'];
}else{
    $user_id = $_SESSION['user_id'];
}

//---- パラメータチェック
if( ! array_key_exists($user_id, $user_profs) ){
    die("無効なアクセス/パラメータです");    
}

$user_prof = $user_profs[$user_id] ;
// ユーザーのデータフォルダー
$user_folder=DATAFILE_PATH .  '/' .  $user_id ;

$user = null;
foreach ($user_profs as $row) {
    if ($row['id'] === $user_id) {
        $user = $row;
        break;
    }
}



if ($_SERVER['REQUEST_METHOD'] === "POST") {
    //
    $tempfile = $_FILES['fname']['tmp_name'];
    $filename =  $user_folder ."/". $_FILES['fname']['name'];
    
    if (is_uploaded_file($tempfile)) {
        if (move_uploaded_file($tempfile, $filename)) {
            setFlashMessage("{$filename}をアップロードしました。");

        } else {
            setFlashMessage( "ファイルをアップロードできませんでした。");
        }
    } else {
        setFlashMessage( "ファイルが選択されていません。または処理中にエラーが発生しました。");
    }
    header("location:?m=upload&id={$user_id}" );
    exit();
}

//
$file_in_dir = get_files_in_dir($user_folder);
// var_dump($file_in_dir);
$flash_message = getFlashMessage();

require("templates/upload.html.php");
