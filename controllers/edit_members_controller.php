<?php

isLogin(true,"ご利用にはログイン認証が必要です。");

if ( array_key_exists('id', $params) ) {
    $user_id = $params['id'];
}else{
    $user_id = $_SESSION['user_id'];
}

// ユーザーのデータフォルダー
$user_folder=DATAFILE_PATH .  '/' .  $user_id ;

// ユーザーの検出
$user = null;
foreach ($user_profs as $row) {
    if ($row['id'] === $user_id) {
        $user = $row;
        break;
    }
}

// パラメータチェック
if( empty($user) ){
    die("パラメータ不正、サポートしないアクセスです");
}
// var_dump($book);

$book_user_info = null;
// 利用ユーザーの特定
foreach ($book_status as $row) {
    if ($row['id'] === $user_id) {
        $book_user_info = $row;
        break;
    }
}
$page['title'] =  $user['name']. "(" . $user['id'] .")";

$book_user = null;
if ($book_user_info) {
    $book_user = $user_profs[$book_user_info['user_id']];
}

// 保存
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $data = $params;  //コピー
    unset($data['m']);
    unset($data['password_c']);

    $data['updated_at']=date("Y-m-d") ."T" . date("H:i:s");
    $data['password']=md5($data['password']);

    $temp = []; //一時バッファ
    foreach ($user_profs as $row) {
        if ($row['id'] === $data['id']) {
            $temp[$row['id']] = $data; //対象レコードは更新
            continue;
        }
        $temp[$row['id']] = $row; //そのまま変更なしでコピー
    }

    $js = json_encode($temp, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    // var_dump($js);//for debug
    // die("test end"); //for debug

    file_put_contents(M_USERS_PROF_PATH, $js, LOCK_EX);
    // バックアップが有効な場合は以下を適用
    // copy(M_USERS_PROF_PATH,DATAFILE_PATH . "/" . "m_user_prof"."_bk" .date('YmdHis') . ".json");
    // copy(M_USERS_PROF_TEMP_PATH,M_USERS_PROF_PATH);
    setFlashMessage("保存しました");
    header("location:?m=edit_members&id=" . $user_id );
    exit();
} else {
    $flash_message = getFlashMessage();

    $file_in_dir = get_files_in_dir($user_folder);

    // echo "<pre>"; print_r($file_in_dir); die();//
    require("templates/edit_members.html.php");
}
