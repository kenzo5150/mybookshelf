<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>

<div class="container">

<h1><?php echo h($user_prof["name"])?></h1>

<?php include_once("flash_message.html.php") ?>

<div class="row">
    <div class="col-sm-6">
    <img   class=" rounded mx-auto d-block;" style="max-height:180px"  src="image.php?url=<?php echo h(DATAFILE_PATH)?>/<?php echo h($user_prof["img"])?>" /><br/>
    </div>

<div class="col-sm-6">
権限: <br/>
<?php  if (hasAdminRole($user_role, $user_id)): ?>
<img style="max-height:64px" src="/image.php?url=static/images/sozai_admin.png" title="管理者"> 管理者
<?php else: ?>
無し
<?php endif ?>
<hr/>
    <ul>
    <li> <a href="/">みんなの書棚</a></li>
    <li> <a href="?m=members">メンバー ダッシュボード</a></li>
    <li> <a href="?m=upload&id=<?php echo h($user_id) ?>">ファイルアップロード</a></li>
    <li> <a href="?m=edit_members&id=<?php echo h($user_id) ?>">ユーザ―情報の編集</a></li>

    </ul>
    </div>
    </div>



<h3 class="DetailTitle" >利用中の書棚/資料 <span style="font-size:80%" ><?php echo h(count($my_book_list)) ?>件</span> </h3>

<?php foreach( $my_book_list as $row ): ?>
    <div style="float:left; width:240px; height:260px;border-bottom:1px solid #eee; padding:1em;margin:1em;">
    <a href="?m=detail&id=<?php echo h($row['id']) ?>"><?php echo h($row['title']) ?><br/>
    <img src="<?php echo h($row['img']) ?>" class=" rounded mx-auto d-block;"   style="max-height:160px;max-width:160px; " />
    </a>
    <br/>
    <div style=""> 
    利用期限 <?php echo h($my_book_status[$row['id']]['date_to']) ?>
    <!--
        <br/>
    更新日時 <?php echo h( substr( str_replace('T',' ', $my_book_status[$row['id']]['updated_at']),0,16)) ?>
    -->
    <br/>
    </div>
</div>
<?php endforeach ?>

<br style="clear: both;"/>


<h3 class="DetailTitle">レビューコメント <span style="font-size:80%" ><?php echo h(count($my_book_comments)) ?>件</span> </h3>

<?php foreach( $my_book_comments as $row ): ?>
    <div style="width:auto; padding:1em;">
    <a href="?m=detail&id=<?php echo h($row['book_id']) ?>">
    <?php echo h($my_book_review_list[$row['book_id']]['title']) ?>
    </a>
<br/>  

<pre><?php echo h($row['comment']) ?></pre>
<div style="background-color: #eee; font-size:80%; text-align:right; padding:6px;border-bottom:1px solid #ddd; ">
投稿日時:<?php echo h(substr( str_replace('T',' ',$row['date_at']),0,16)) ?>
    </div>
    </div>
<?php endforeach ?>



</div>
<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    