<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>

<div class="container">

<h1><?php echo h($page['title']) ?></h1>


<?php include_once("flash_message.html.php") ?>

<div class="row">
<div class="col-sm-2">
    <a class="btn btn-primary d-block" href="./?m=members">戻る</a>
</div>    
</div>    
    
<hr/>



<div class="row">
    <div class="col-sm-6">

    
<form action="index.php" method="post" onsubmit="return checkMemberFormInput()" >
<input type="hidden" name="m" value="edit_members">
<input type="hidden" name="id" id="user_id" value="<?php echo h($user['id'])?>">
<input type="hidden" id="DATAFILE_PATH" value="<?php echo h(DATAFILE_PATH) ?>"/>
    <div class="form-group">
    <label for="formInputName">    名前:</label>
    <input type="text" name="name" required class="form-control" value="<?php echo h($user['name'])?>" />
    </div>

    <div class="form-group">
    <label for="formInputImg">    アイコン画像: <a class="btn btn-sm " style="font-size:80%" href="javascript:void(0)" onclick="alert('ファイルアップロード機能から追加できます')">※画像の追加について</a></label>
    <input type="text" name="img" id="img" required class="form-control" value="<?php echo h($user['img'])?>" />
    </div>

    <?php if( count($file_in_dir) > 0 ): ?>
    <div class="form-group">
    <label for="formInputImg"><a href="javascript:void(0)" onclick="$('#file_select').toggle()">保存ファイルから選択</a>: <?php echo h(count($file_in_dir)) ?> files </label> 
    <select id="file_select" size="6"  class="form-control" onchange="preview_selected()" style="display:none">

    <?php foreach($file_in_dir as $f ): ?>
        <option value="<?php echo h($user_folder."/".$f) ?>"><?php echo h($f ) ?> ( <?php echo date ("Y/m/d H:i:s", filemtime($user_folder."/".$f));?> , size: <?php echo filesize($user_folder."/".$f)/1000;?>kb )</option>

    <?php endforeach; ?>
    </select>
    
    <div id="imagePreviewPanel" style="display:none">
    <img id="imagePreview" src="" class="rounded" style="border:1px solid #ddd; margin:1em; max-width:120px; max-height:120px; display:block" />
    <a href="javascript:void(0)" class="btn btn-primary btn-sm d-block" onclick="changeInput('img')" id="kakutei_input" data-input="">確定</a>
    <a href="javascript:void(0)" class="btn btn-danger btn-sm d-block" onclick="cancelInput()" id="kakutei_cancel" style="margin-top:1em">cancel</a>
    </div>
    </div>
    <?php endif; ?>
    


    <div class="form-group">
    <label for="formInputMail">    Eメール:</label>
    <input type="text" name="mail" required class="form-control" value="<?php echo h($user['mail'])?>" />
    </div>

    <div class="form-group">
    <label for="formInputPasdword">    パスワード:</label>
    <input type="password" name="password" id="formInputPasdword" required minlength="6" maxlength="16" class="form-control" value="" />
    </div>

    <div class="form-group">
    <label for="formInputPasdwordC">    パスワード(確認用):</label>
    <input type="password" name="password_c" id="formInputPasdwordC" required minlength="6" maxlength="16" class="form-control" value="" />
    </div>

    <div class="form-group">
    <label for="formInputNotes">    登録日時:</label>
    <input  type="datetime-local" name="registed_at"  class="form-control" value="<?php echo (array_key_exists('registed_at', $user))? h($user['registed_at']) : '' ?>" />
    </div>


    <div class="form-group">
    <label for="formInputNotes">    最終更新日時:</label>
    <input  type="datetime-local" name="updated_at"  class="form-control" value="<?php echo (array_key_exists('updated_at', $user))? h($user['updated_at']) : '' ?>" />
    </div>

    <div class="form-group">
    <label for="formInputNotes">    備考:</label>
    <textarea name="notes"  class="form-control" ><?php echo (array_key_exists('notes', $user))? h($user['notes']) : '' ?></textarea>
    </div>

<?php if(hasAdminRole($user_role, $_SESSION['user_id'])) : ?> 
    <div class="form-group">
    <button type="submit" class="btn btn-primary col-12">更新</button>
    </div>
<?php else: ?>
    <span style="color:red; font-weight:bolder;">ユーザー自身または管理者のみ更新可能です </span>
<?php endif ?> 

    <div class="form-group">
    <a class="btn btn-secondary  col-12" href="?m=members">キャンセル</a></br>
    </div>

    </div>


</div>

<hr>
</div>

<script >

function checkMemberFormInput(){

    var res = false ;
    if( $('#formInputPasdword').val() !== $('#formInputPasdwordC').val()  ){
        alert("パスワードと確認用が一致しません");
        return res
    }
    res = confirm("更新してよろしいでしょうか？");
    return res;
}


function preview_selected(){
    var select_image = $('#file_select').val() ;
    var DATAFILE_PATH = $('#DATAFILE_PATH').val() ; //../form_app-data' ;

    var user_id = $('#user_id').val();
    var img_src_url="./image.php?url=" + select_image;
    var extend = select_image.match(/[^.]+$/);
    var extend_l = extend[0].toLowerCase() ;
    // ../form_app-data/
    if( extend_l === "jpg" ||   extend_l === "png" ||   extend_l === "jpeg" ||  extend_l === "gif" ){

    }   else{
        return ;
    }

    $('#imagePreviewPanel').hide();
    $('#imagePreview').attr('src', img_src_url);
    var new_url= select_image.replace( DATAFILE_PATH + "/" , '');
    // console.log( new_url );
    $('#imagePreviewPanel').show();
    var result = $('#kakutei_input').data('num',new_url);

    var result_c = $('#kakutei_input').data('num');
    console.log( result_c );

}

function changeInput(id){
    var result_c = $('#kakutei_input').data('num');
    console.log( result_c );
    if( confirm ("アイコン画像を変更しますか？ " + "\n" + result_c ) ) {
        $('#' + id ).val(result_c);
    }
}

function cancelInput(){
    $('#imagePreviewPanel').hide();
}
</script>

<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    
