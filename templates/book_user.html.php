<div class="col-sm-6" >
    <h3 class="DetailTitle">現在の利用者</h3>

    <?php if ($book_user): ?>

    <?php echo h($book_user['name']) ?>
    <br/>
    <img class="rounded mx-auto d-block"  src="image.php?url=<?php echo  h( DATAFILE_PATH."/".$user_profs[$book_user_info['user_id']]['img'] )  ?>" style="max-height:180px" />
    <br/>
    <span style="color:red" >利用期限:</span><?php echo h($book_user_info['date_to']) ?>
    <?php else: ?>
    <span style="color:green">現在、利用者無し</span>
    <?php endif; ?>

    <a class="btn btn-warning" href="javascript:void(0)" onclick="$('#form_change_book_status').toggle('slow')" >変更</a>

        <div class="form-group" id="form_change_book_status" style="display:none">
    <form action="./" method="post" enctype="multipart/form-data" onsubmit="">
    <input type="hidden" name="m" value="change_book_status" >
    <input type="hidden" name="id" value="<?php echo h($book['id'])?>" >

    <div class="form-group">
    <label for="formInputDateTo">    利用期限：</label>
    <input type="date" name="date_to" class="form-control" value="" />
    </div>

    <label for="formInputDateTo">    利用者:</label>
    <div class="form-group">
    <select name="user_id"  class="form-control" >
        <option>- 利用者を選択してください。 - </option>
        <?php foreach($user_profs as $row): ?>
            <option value="<?php echo h($row['id']) ?>" > <?php echo h($row['name']) ?></option>
            <?php endforeach ?>
        
    </select>
    </div>
    <br/>

    <input class="btn btn-warning d-block" type="submit" value="利用者変更" />

    <br/>
    <a href="javascript:void(0)" onclick="$('#coution_notes').toggle('slow')"> 注意事項</a>
        <div id="coution_notes" onclick="$(this).toggle()" style="padding:4px; font-size:80%; border:1px solid #ddd; border-radius: 6px;; color:red; display:none">利用者無に設定する場合は期限、利用者を非選択にする</div>

    </form>
    </div>


</div>