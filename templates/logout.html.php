<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>

<div class="container">

<h1><?php echo h($page['title']) ?></h1>

<?php include_once("flash_message.html.php") ?>
    
<hr/>

<form action="index.php" method="post" onsubmit="return confirm('本当にログアウトしますか？');">
  <input type="hidden" name="m" value="logout">
  <button type="submit" class="btn btn-primary col-12">ログアウトする</button>
</form>

<hr>
</div>
<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    
