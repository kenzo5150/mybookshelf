<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>
<?php //print_r($book_status) ?>

<div class="container">

<h1></h1>

<?php include_once("flash_message.html.php") ?>


<div class="row">
<div class="col-sm-2">
    <a class="btn btn-primary d-block"href=./">Back</a>
</div>    
</div>    
    
<hr/>



<div class="row">
    <div class="col-sm-6">

    
<form action="index.php" method="post">
<input type="hidden" name="m" value="edit">
<input type="hidden" name="id" value="<?php echo h($book['id'])?>">

    <div class="form-group">
    <label for="formInputTitle">    タイトル:</label>
    <input type="text" name="title" required class="form-control" value="<?php echo h($book['title'])?>" />
    </div>

    <div class="form-group">
    <label for="formInputImg">    タイトル画像:</label>
    <input type="text" name="img" required class="form-control" value="<?php echo (isset( $book['img']))? h($book['img']) : '' ?>" />
    </div>

    <div class="form-group">
    <label for="formInputUrl">    出版社サポートURL:</label>
    <input type="text" name="url" required class="form-control" value="<?php echo (isset( $book['url']))? h($book['url']) : '' ?>" />
    </div>

    <div class="form-group">
    <label for="formInputUrl">    出版社/発案者:</label>
    <input  type="text" required name="publisher"  class="form-control" value="<?php echo (isset( $book['publisher']))? h($book['publisher']) : '' ?>" />
    </div>

    <div class="form-group">
    <label for="formInputNotes">    提供開始日時:</label>
    <input  type="datetime-local" required name="released_at"  class="form-control" value="<?php echo (isset ($book['released_at']))? h($book['released_at']) : '' ?>" />
    </div>


    <div class="form-group">
    <label for="formInputNotes">    最終更新日時:</label>
    <input  type="datetime-local" name="updated_at"  class="form-control" value="<?php echo (isset ($book['updated_at']))? h($book['updated_at']) : '' ?>" />
    </div>

    <div class="form-group">
    <label for="formInputNotes">    備考:</label>
    <textarea name="notes"  class="form-control" ><?php echo h($book['notes'])?></textarea>
    </div>


    <div class="form-group">
    <button type="submit" class="btn btn-primary col-12">更新</button>
    </div>

    <div class="form-group">
    <a class="btn btn-secondary  col-12" href="?m=detail&id=<?php echo h($book['id'])?>">キャンセル</a></br>
    </div>

    </div>


</div>

<hr>
</div>
<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    
