<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>

<div class="container">

<h1><?php echo h($page['title']) ?></h1>

<?php include_once("flash_message.html.php") ?>
    

<?php if( isLogin()): ?>
  すでにログインしています。
  <br/>別のユーザーアカウントでログインしなおす場合はログアウトしてください
  <br/>
  <div ></div><a class="btn btn-primary" href="./?m=logout">ログアウト</a>
<?php else: ?>
   ID,パスワードを入力しログインしてください
  
<hr/>

<form action="index.php" method="post">
  <input type="hidden" name="m" value="login">
  <div class="form-group">
    <label for="formInputUserId1">ユーザーID</label>
    <input name="user_id" required class="form-control" id="formInputUserId1" aria-describedby="UserIdHelp" placeholder="ユーザーIDを入力">
  </div>
  <div class="form-group">
    <label for="formInputPassword1">パスワード</label>
    <input type="password" required name="password" class="form-control" id="formInputPassword1" placeholder="パスワードを入力">
  </div>
  <button type="submit" class="btn btn-primary col-12">Login</button>
</form>
<?php endif ?>

<hr>
</div>
<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    
