<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>
<?php //print_r($book_status) ?>

<div class="container">

<h1></h1>
<?php include_once("flash_message.html.php") ?>

<div class="row">
<div class="col-sm-2">
    <a class="btn btn-primary d-block" href="./">Back</a>
</div>    
</div>    
    
<hr/>


<div class="row">
    <div class="col-sm-6">
    <h3 class="DetailTitle"><?php echo h($page['title']) ?></h3>

    <form>
        <div class="form-group">
        <img class="rounded mx-auto d-block" style="max-width: 400px; " src="<?php echo h($book['img'])?>" style="" />
        </div>

        <div class="form-group">
        <a href="<?php echo h($book['url'])?>" target="_blank" onclick="return confirm('外部サイトを開きます。よろしいですか？')" >サポートページURL</a>
        </div>

    
        <div class="form-group">

        提供開始日時         <?php echo (isset($book['released_at']))? h( strftime("%Y-%m-%d %H:%M",strtotime($book['released_at']))) : '' ?>
        </div>

        <div class="form-group">


        最終更新日時   <?php echo (isset( $book['updated_at']))? h(strftime("%Y-%m-%d %H:%M",strtotime($book['updated_at']))) : '' ?>
        </div>

        <div class="form-group">

            備考<br/>
            <div style="width:100%; border-radius:4px ; border: 1px solid #ccc; padding:0.6em;overflow-wrap: break-word;
 word-break: break-all;">
            <?php if (isset($book['notes'])) :?>
                <pre><?php echo h($book['notes'])?></pre>
                <?php else: ?>
                    －なし－
                <?php endif ?>
            </div>
        </div>

        <div class="col-sm-12">
        <a class="btn btn-success d-block" href="?m=edit&id=<?php echo h($book['id'])?>">編集</a></br>
        </div>
            </form>   
    </div>

    <?php require("book_user.html.php");?>
</div>

<hr>

<div class="row">
    <div class="col-sm-12">
<h3 class="DetailTitle">レビューコメント</h3>

<?php 
$review_list= [];
if( array_key_exists( $book['id'] , $book_review) )
{  
    echo ( "(" . count( $book_review[$book['id']]) ) ."件)";
    $review_list= $book_review[$book['id']];
    $sort_datas = [];
    // $sort_datas = array_column($review_list, 'date_at');
    foreach($review_list as $x) {
        $sort_datas[] = $x['date_at'];
    }
    // ソートする投稿日時降順 新しいものが上位
    array_multisort($sort_datas, SORT_DESC, $review_list);

} 
else 
{ 
    echo "<span style='color:red;font-size:90%'>まだ登録されていません</span>";
} 
?>
    </div>
</div>


<div class="row">
<div class="col-sm-12">
最大入力文字数: 250文字
    <form action="./" method="post" enctype="multipart/form-data" onsubmit="return checkBookReviewForm()">
    <input type="hidden" name="m" value="add_book_review" >
    <input type="hidden" name="book_id" value="<?php echo h($book['id'])?>" >
    <textarea class="form-control" id="comment" minlength="10" style="width:300px; height:10em;" maxlength="250" name="comment"></textarea>
    <br/>
    <input class="btn btn-warning d-block" type="submit" value="レビュー登録" />
    </form>
    </div>
</div>


<hr>

<?php foreach(  $review_list as $row ) : ?>
    <pre><?php echo h($row['comment'] )  ?></pre>
    
    <br/>
    <div style="text-align: right;">
    投稿者:<?php echo h( $user_profs[$row['user_id']]['name'] )  ?> <?php echo h( substr( str_replace("T", " ", $row['date_at']) ,0,16))  ?> 
    </div>
    <hr>
<?php endforeach; ?>
     

</div>

<script>
    function checkBookReviewForm(){
        console.log( $('#comment').val().length );
        if(  $('#comment').val().length < 10 ){
            alert("10文字以上~250文字以下で入力してください")
            return false;
        }
        return confirm("登録しますか？");
    }

</script>
<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    
