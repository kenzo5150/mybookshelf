<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>

<div class="container">
<h1><?php echo h($page['title']) ?></h1>

<?php include_once("flash_message.html.php") ?>


<div class="row">
<div class="col-sm-8">
<form action="./" method="GET">
<label >タイトル:</label><input name="k_title" id="k_title" class="form-control" value="<?php echo h( (array_key_exists('k_title', $params))? $params['k_title'] : "") ?>" /> 
<label>備考:</label><input name="k_notes" id="k_notes" class="form-control" value="<?php echo h( (array_key_exists('k_notes', $params))? $params['k_notes'] : "") ?>" /> 
<input class="btn btn-warning form-control" type="submit" value="find" /> 
<input class="btn btn-secondary form-control" type="button" value="clear" onclick="$('#k_title').val('');$('#k_notes').val('');" /> 
</form>
</div>


<div class="col-sm-4">
<?php if( count($result_list) >0 ) : ?>
該当:<?php echo h( count($result_list)) ?>件
<?php else: ?>
該当:なし
<?php endif ?>
<br/>
登録総数:<?php echo h( count($books)) ?>件
<br/>

<a href="./?m=add_book" class="btn btn-primary d-block">新規登録</a>
</div>


</div>
<hr>



    <?php 
    $line_num=0;
    foreach ($result_list as $item) : 
        $line_num++;
    
    ?>
            
    <div class="card" style=" margin: 1em 0px;">

        <div class="card-header">
            <div class="card-title"><a class="d-block" href="./?m=detail&id=<?php echo h($item['id']) ?>" class=""><?php echo h($line_num) ?>. <?php echo h($item['title']) ?></a></div>
        </div>

                        
                        
        <div class="card-body">
            
        <div class="card-text">
            <div class="row">

            <div class="col-sm-6">
            <a href="./?m=detail&id=<?php echo h($item['id']) ?>" class="">
            <?php if($item['img']): ?>
            <img class=" rounded mx-auto d-block img-fluid" src="<?php echo (  $item['img'] ) ?>" style="border:1px solid #ccc; max-width:120px;"/>
            <?php else: ?>
            詳細
            <?php endif ?>
            
            </a></br>

            </div>
            <div class="col-sm-4">
            <?php if ( !empty( $item['url']) ): ?>
                    <a  class="btn btn-secondary btn-block" onclick="return confirm('外部サイトを開きます。よろしいでしょうか？')" href="<?php echo h($item['url']); ?>" target="_blank" >サポートページ</a> 
                    <?php endif ?>

            <br/>
            提供開始:<?php echo (isset( $item['released_at']))? h(strftime("%Y-%m-%d",strtotime($item['released_at']))) : ''  ?>
                        <br/>
            最終更新:<?php echo (isset( $item['updated_at']))?  h(strftime("%Y-%m-%d",strtotime($item['updated_at']))) : ''  ?>
            <br/>
            <?php if($item['notes']): ?>
                <div style="width:auto; min-height:4em;border-radius:4px ; border: 1px solid #ccc; padding:0.6em;overflow-wrap: break-word; word-break: break-all;">
                <pre><?php echo h(mb_substr($item['notes'],0,50)) ?></pre>
                <?php echo (mb_strlen($item['notes'])>50)? "..." : "" ?>
            </div>
                <br/>
            <?php endif ?>
                レビューコメント:
            <?php 
            if( array_key_exists( $item['id'] , $book_review) ){
                echo ( count( $book_review[$item['id']]) ) ."件"; 
            } else{
                echo "(なし)";
            }
            ?>




        <div class="card-text">
            <?php 
            //print_r($book_status)
            $img="";
            $img_url="";
            $user_info="";
            foreach($book_status as $row):

                if($row['id'] === $item['id']) {
                    // echo "[貸出中]";
                    // echo $user_profs [ $row['user_id'] ] ;
                    $is_owner = ($row['user_id']===$_SESSION['user_id'])? "{$user_profs[ $row['user_id'] ]["name"] } <b style='color:red'>自分</b>":"{$user_profs[ $row['user_id'] ]["name"] }";
                    $user_info = "現在の利用者: ". $is_owner;
                    $user_id=$row['user_id'];
                    $img = $user_profs[ $row['user_id'] ]["img"] ;
                    $img_url = DATAFILE_PATH."/". $img ;
                    break;
                }
            ?>
            <?php            
            endforeach;                        
            ?>

        <?php if(!empty($user_info)) { ?>
            <hr />
            <?php echo ($user_info) ?>
            <?php  } ?> 
        </div>
        </div>
        </div>


        </div>
        </div>
        </div>
<?php endforeach; ?>


</div>
</div>
</div>


</div>

            </div>

<div id="space" style="height:80px"></div>            
<div class="Footer"><div  style="text-align:right"><a class="btn btn-warning" href="#header">Top</a></div></div>

<script type="text/javascript">
 // ページ内リンクのみ取得
  var scroll = new SmoothScroll('a[href*="#"]', {
   speed: 100,//スクロールする速さ
   header: '#header'//固定ヘッダーがある場合
  });
 </script>

<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    