<?php include_once("html_header.html.php") ?>

<?php include_once("nav.html.php") ?>

<div class="container">

<h1><?php echo h($page['title']) ?></h1>

<?php include_once("flash_message.html.php") ?>
    
<?php if(hasAdminRole($user_role, $_SESSION['user_id'])): ?>
<a class="btn btn-primary" href="?m=add_members" >新規登録</a>
<?php endif ?>

<div class=row >
<?php 
$cnt=0;
foreach($user_profs as $row): 

?>
    <div class="col-sm-2" style=" padding:0.4em; margin: 0.2em; border: 1px solid #ddd; border-radius: 4px; ;">
      <a href="javascript:void(0)" onclick="$('#member_menu_<?php echo h($row['id']) ?>').toggle()" ><?php echo $row['name'] ?><br/>
      <img class="img-fluid" style="max-height:100px;" src="./image.php?url=<?php echo h(DATAFILE_PATH) ?>/<?php echo $row['img'] ?>" />
      </a>
      <div id="member_menu_<?php echo h($row['id']) ?>" style="display:none">
      <ul style="font-size:90%">
      <li><a href="?m=mypage&id=<?php echo h($row['id']) ?>" >マイページ</a></li>
      <li><a href="?m=upload&id=<?php echo h($row['id']) ?>" >ファイルアップロード</a></li>
      <li><a href="?m=edit_members&id=<?php echo h($row['id']) ?>" >ユーザ―情報</a></li>
      </ul>
      </div>
    </div>

<?php endforeach ?>
</div>

<br style="clear: both"/>

</div>
<?php include_once("dialog.html.php") ?>

<?php include_once("scripts.html.php") ?>

    
