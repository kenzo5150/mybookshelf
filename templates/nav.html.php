<nav class="navbar navbar-expand-lg navbar-light bg-light">

  <a id="header" class="navbar-brand" href="/"><img style="max-height:64px" src="/image.php?url=static/images/Mammoth-Seated-icon_128.png"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <?php if(isLogin()): ?>
          <a class="nav-link" href="./?m=mypage">マイページ</a>
          <?php else : ?>
          <a class="nav-link" href="./?m=login">ログイン</a>
          <?php endif ?>
      </li>
      <li class="nav-item active">
        <?php if(isLogin()): ?>
          <a class="nav-link"  href="?m=members">メンバー ダッシュボード</a>
          <?php else : ?>
          <?php endif ?>
      </li>
      <li class="nav-item dropdown">
      <?php if(isLogin()): ?>
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        ページ選択
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="./">みんなの書棚 TOP</a>
          <a class="dropdown-item"  href="?m=upload">ファイルアップロード</a>
          <a class="dropdown-item"  href="?m=edit_members">ユーザ―情報の編集</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="./?m=logout">ログアウト</a>

        </div>
        <?php endif ?>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" style="display: none;" aria-disabled="true">Disabled</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" style="display: none;" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" style="display: none;" type="submit">Search</button>
    </form>
  </div>
</nav>