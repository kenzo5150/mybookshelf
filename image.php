<?php
$image_url=$_GET['url'];

// if( file_exists("./static/images/" . $image_url) ){
//     echo "ok";
// } else {
//     echo "ng:" . getcwd();
// }

// exit();

$filename="" . $image_url;

$file_extension = strtolower(substr(strrchr($filename,"."),1));

switch ($file_extension) {
    case "pdf": $ctype="application/pdf"; break;
    case "exe": $ctype="application/octet-stream"; break;
    case "zip": $ctype="application/zip"; break;
    case "doc": $ctype="application/msword"; break;
    case "xls": $ctype="application/vnd.ms-excel"; break;
    case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
    case "gif": $ctype="image/gif"; break;
    case "png": $ctype="image/png"; break;
    case "jpe": case "jpeg":
    case "jpg": $ctype="image/jpg"; break;
    case "jpeg": $ctype="image/jpg"; break;
    default: $ctype="application/force-download";
}

header('Content-Disposition: inline; filename="' . $filename . '"', true);
header("Content-Type: $ctype");
readfile( $filename);
// exit();


