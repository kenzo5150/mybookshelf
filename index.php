<?php
session_start();
require_once("lib/utils.php");

// 書籍一覧マスタ等のファイル(JSON)保管パス
define('DATAFILE_PATH', 'data');//公開ディレクトリに収める。デモ用
// define('DATAFILE_PATH', '../data');// 階層を上位に配置し、公開ディレクトリに置かない
define('T_BOOKS_STATUS_PATH', DATAFILE_PATH . "/" . "t_book_status.json");
define('M_BOOKS_TEMP_PATH', DATAFILE_PATH . "/" . "m_book_temp.json");
define('M_BOOKS_PATH', DATAFILE_PATH . "/" . "m_book.json");
define('T_BOOKS_REVIEW_PATH', DATAFILE_PATH . "/" . "t_book_review.json");
define('M_USERS_PROF_PATH', DATAFILE_PATH . "/" . "m_user_prof.json");
define('M_USERS_PROF_TEMP_PATH', DATAFILE_PATH . "/" . "m_user_prof_temp.json");
define('M_USERS_ROLE_PATH', DATAFILE_PATH . "/" . "m_user_role.json");


$books =  json_decode(file_get_contents(M_BOOKS_PATH), true);


//$url_book_status="data/book_status.json";
$book_status =  json_decode(file_get_contents(T_BOOKS_STATUS_PATH), true);
$book_review_list =  json_decode(file_get_contents(T_BOOKS_REVIEW_PATH), true);
// var_dump($book_review_list);

$book_review =  list_summarize($book_review_list, 'book_id');
//  $book_status=$temp['book_status'];
// echo "<pre>";
// var_dump($book_review);
// die();


$data_dir = "./data";
$users = []; // ["u001","u002","u003","u004"]のような配列

// 有効メンバはファイルから取得
$user_profs = json_decode(file_get_contents(M_USERS_PROF_PATH), true);
$user_role = json_decode(file_get_contents(M_USERS_ROLE_PATH), true);

// print "<pre>";var_dump($user_role);die();
// var_dump($user_profs);

$m = "";

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $params = $_GET;

    if (!array_key_exists("m", $params)) {
        require_once("controllers/index_controller.php");
    } elseif (($params['m'] === "index" || $params['m'] === '')) {
        $m = "index";
        require_once("controllers/index_controller.php");
    } elseif ($params['m'] === "detail") {
        $m = "detail";
        require_once("controllers/detail_controller.php");
    } elseif ($params['m'] === "edit") {
        require_once("controllers/edit_controller.php");
    } elseif ($params['m'] === "login") {
        require_once("controllers/login_controller.php");
    } elseif ($params['m'] === "logout") {
        require_once("controllers/logout_controller.php");
    } elseif ($params['m'] == "form1") {
        $m = "form1";
        // echo $m .PHP_EOL;
        require_once("controllers/form1_controller.php");
    } elseif ($params['m'] == "mypage") {
        require_once("controllers/mypage_controller.php");
    } elseif ($params['m'] == "members") {
        require_once("controllers/members_controller.php");
    } elseif ($params['m'] == "edit_members") {
        require_once("controllers/edit_members_controller.php");
    } elseif ($params['m'] == "book_review") {
        require_once("controllers/book_review_controller.php");
    } elseif ($params['m'] == "add_book") {
        require_once("controllers/add_book_controller.php");
    } elseif ($params['m'] == "add_members") {
        require_once("controllers/add_members_controller.php");
    } elseif ($params['m'] == "upload") {
        require_once("controllers/upload_controller.php");
    } elseif ($params['m'] == "change_book_status") {
        require_once("controllers/change_book_status_controller.php");
    } else {
        die("許可されていないアクセスです");
    }

    // echo $m .PHP_EOL;
} elseif ($_SERVER['REQUEST_METHOD'] == "POST") {
    $params = $_POST;
    if (!array_key_exists("m", $params)) {
        die("param error");
    } elseif (($params['m'] === "form1_confirm")) {
        require_once("controllers/form1_confirm_controller.php");
    } elseif (($params['m'] === "edit")) {
        require_once("controllers/edit_controller.php");
    } elseif ($params['m'] === "login") {
        require_once("controllers/login_controller.php");
    } elseif ($params['m'] === "logout") {
        require_once("controllers/logout_controller.php");
    } elseif (($params['m'] === "form1_save")) {
        require_once("controllers/form1_save_controller.php");
    } elseif ($params['m'] == "members") {
        require_once("controllers/members_controller.php");
    } elseif ($params['m'] == "edit_members") {
        require_once("controllers/edit_members_controller.php");
    } elseif ($params['m'] == "book_review") {
        require_once("controllers/book_review_controller.php");
    } elseif ($params['m'] == "upload") {
        require_once("controllers/upload_controller.php");
    } elseif ($params['m'] == "add_book_review") {
        require_once("controllers/add_book_review_controller.php");
    } elseif ($params['m'] == "add_book") {
        require_once("controllers/add_book_controller.php");
    } elseif ($params['m'] == "add_members") {
        require_once("controllers/add_members_controller.php");
    } elseif ($params['m'] == "change_book_status") {
        require_once("controllers/change_book_status_controller.php");
    } else {
        die("許可されていないアクセスです");
    }
}
